﻿using BuckarooSampleMVC.Helpers;
using BuckarooSampleMVC.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BuckarooSampleMVC.Controllers
{
    public class BuckarooController : Controller
    {
        private readonly ILogger<BuckarooController> _logger;

        public BuckarooController(ILogger<BuckarooController> logger)
        {
            _logger = logger;
        }
        public ActionResult Success()
        {
            return View();
        }
        public ActionResult Error()
        {
            var formData = HttpContext.Request.Form; // example: formData["brq_amount"]

            return View();
        }
        [HttpPost]
        public ActionResult PushResponseSuccess([FromBody] BuckarooPushResponseModel buckarooPushModel)
        {
            return View(buckarooPushModel);
        }
        [HttpPost]
        public ActionResult PushResponseFailure([FromBody] BuckarooPushResponseModel buckarooPushModel)
        {
            return View(buckarooPushModel);
        }
        [HttpGet]
        public async Task<ActionResult> Pay()
        {
            var amount = 1;
            var appUrl = "https://localhost:44394";
            PaymentRequest paymentRequest = new()
            {
                ContinueOnIncomplete = (byte)Helpers.Enums.ContinueOnIncomplete.RedirectToHTML, // if the service selection will be made by the client otherwise 'No'
                ServicesSelectableByClient = "ideal,mastercard,visa,maestro",
                ServicesExcludedForClient = null,
                Currency = "EUR",
                AmountDebit = (decimal)amount,
                Invoice = Guid.NewGuid().ToString(),
                ClientIP = new ClientInfo { Address = "0.0.0.0", Type = (byte)Helpers.Enums.IPProtocolVersion.IPv4 },
                ReturnURL = appUrl + "/Buckaroo/Success",
                ReturnURLCancel = appUrl + "/Buckaroo/Error",
                ReturnURLError = appUrl + "/Buckaroo/Error",
                ReturnURLReject = appUrl + "/Buckaroo/Error",
                CustomParameters = null,
                AdditionalParameters = null,
                PushURL = appUrl + "/Buckaroo/PushResponseSuccess",
                PushURLFailure = appUrl + "/Buckaroo/PushResponseFailure",
                Services = null // new Services { ServiceList = new List<Service>() { new Service { Name = "ideal", Action = "Pay" } } }
            };
            Actions buckarooClient = new();
            PaymentResponse paymentResponse = await buckarooClient.Pay(paymentRequest);

            return Json(paymentResponse);
        }
    }
}
