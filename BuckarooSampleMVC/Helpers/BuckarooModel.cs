﻿using System;
using System.Collections.Generic;

namespace BuckarooSampleMVC.Helpers
{
    public class PaymentRequest
    {
        public byte ContinueOnIncomplete { get; set; }
        public string ServicesSelectableByClient { get; set; }
        public string ServicesExcludedForClient { get; set; }
        public string Currency { get; set; }
        public decimal AmountDebit { get; set; }
        public string Invoice { get; set; }
        public ClientInfo ClientIP { get; set; }
        public Services Services { get; set; }
        public string ReturnURL { get; set; }
        public string ReturnURLCancel { get; set; }
        public string ReturnURLError { get; set; }
        public string ReturnURLReject { get; set; }
        public string PushURL { get; set; }
        public string PushURLFailure { get; set; }
        public object CustomParameters { get; set; }
        public AdditionalParameters AdditionalParameters { get; set; }
    }
    public class ClientInfo
    {
        public byte Type { get; set; }
        public string Address { get; set; }
    }
    public class Services
    {
        public List<Service> ServiceList { get; set; }
    }
    public class Service
    {
        public string Name { get; set; }
        public string Action { get; set; }
        public List<ServiceParameter> Parameters { get; set; }
    }
    public class ServiceParameter
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
    public class AdditionalParameters
    {
        public List<ServiceParameter> AdditionalParameter { get; set; }
    }

    public class PaymentResponse
    {
        public string Key { get; set; }
        public Status Status { get; set; }
        public RequiredAction RequiredAction { get; set; }
        public object Services { get; set; }
        public object CustomParameters { get; set; }
        public object AdditionalParameters { get; set; }
        public object RequestErrors { get; set; }
        public string Invoice { get; set; }
        public object ServiceCode { get; set; }
        public bool IsTest { get; set; }
        public string Currency { get; set; }
        public float AmountDebit { get; set; }
        public object TransactionType { get; set; }
        public int MutationType { get; set; }
        public object RelatedTransactions { get; set; }
        public object ConsumerMessage { get; set; }
        public object Order { get; set; }
        public object IssuingCountry { get; set; }
        public bool StartRecurrent { get; set; }
        public bool Recurring { get; set; }
        public object CustomerName { get; set; }
        public object PayerHash { get; set; }
        public object PaymentKey { get; set; }
    }
    public class Status
    {
        public StatusCode Code { get; set; }
        public StatusCode SubCode { get; set; }
        public DateTime DateTime { get; set; }
    }
    public class StatusCode
    {
        public int Code { get; set; }
        public string Description { get; set; }
    }
    public class RequiredAction
    {
        public string Name { get; set; }
        public int TypeDeprecated { get; set; }
        public string RedirectURL { get; set; }
        public List<RequestedInformation> RequestedInformation { get; set; }
        public PayRemainderDetails PayRemainderDetails { get; set; }
    }
    public class PayRemainderDetails
    {
        public decimal RemainderAmount { get; set; }
        public string Currency { get; set; }
        public string GroupTransaction { get; set; }
    }
    public class RequestedInformation
    {
        public string Name { get; set; }
        public byte DataType { get; set; }
        public int MaxLength { get; set; }
        public bool Required { get; set; }
        public string Description { get; set; }
    }
}
