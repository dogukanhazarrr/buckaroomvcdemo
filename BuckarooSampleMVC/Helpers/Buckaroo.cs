﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BuckarooSampleMVC.Helpers
{
    public class Actions
    {
        private readonly string apiBaseAddress = "https://testcheckout.buckaroo.nl/";

        public async Task<PaymentResponse> Pay(PaymentRequest paymentRequest)
        {
            CustomDelegatingHandler customDelegatingHandler = new();
            HttpClient client = HttpClientFactory.Create(customDelegatingHandler);

            HttpResponseMessage response = await client.PostAsJsonAsync(apiBaseAddress + "json/transaction", paymentRequest);
            var result = response.Content.ReadAsStringAsync().Result;

            PaymentResponse paymentResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<PaymentResponse>(result);

            return paymentResponse;
        }
    }

    public class CustomDelegatingHandler : DelegatingHandler
    {
        //Obtained from the server earlier
        private readonly string websiteKey = "OckUOFtBqu";
        private readonly string secretKey = "TyGLC3po3ttZToTviUZeBZat";

        protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            string requestContentBase64String = string.Empty;

            string requestUri = System.Web.HttpUtility.UrlEncode(request.RequestUri.Authority + request.RequestUri.PathAndQuery).ToLower();

            string requestHttpMethod = request.Method.Method;

            // calculate UNIX time
            DateTime epochStart = new(1970, 01, 01, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan timeSpan = DateTime.UtcNow - epochStart;
            string requestTimeStamp = Convert.ToUInt64(timeSpan.TotalSeconds).ToString();

            // create random nonce for each request
            string nonce = Guid.NewGuid().ToString("N");

            // checking if the request contains body, usually will be null with HTTP GET and DELETE
            if (request.Content != null)
            {
                byte[] content = await request.Content.ReadAsByteArrayAsync(cancellationToken);
                MD5 md5 = MD5.Create();

                // hashing the request body, any change in request body will result in different hash, we'll incure message integrity
                byte[] requestContentHash = md5.ComputeHash(content);
                requestContentBase64String = Convert.ToBase64String(requestContentHash);
            }

            // creating the raw signature string
            string signatureRawData = String.Format("{0}{1}{2}{3}{4}{5}", websiteKey, requestHttpMethod, requestUri, requestTimeStamp, nonce, requestContentBase64String);

            var secretKeyByteArray = Encoding.UTF8.GetBytes(secretKey);

            byte[] signature = Encoding.UTF8.GetBytes(signatureRawData);

            using (HMACSHA256 hmac = new(secretKeyByteArray))
            {
                byte[] signatureBytes = hmac.ComputeHash(signature);
                string requestSignatureBase64String = Convert.ToBase64String(signatureBytes);

                // setting the values in the Authorization header using custom scheme (hmac)
                request.Headers.Authorization = new AuthenticationHeaderValue("hmac", string.Format("{0}:{1}:{2}:{3}", websiteKey, requestSignatureBase64String, nonce, requestTimeStamp));
            }

            // set other headers
            request.Headers.Add("culture", "en-US");
            request.Headers.Add("channel", "Web");

            HttpResponseMessage response = await base.SendAsync(request, cancellationToken);

            return response;
        }
    }
}
