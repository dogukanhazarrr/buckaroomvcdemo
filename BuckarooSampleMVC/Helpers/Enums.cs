﻿namespace BuckarooSampleMVC.Helpers.Enums
{
        enum ContinueOnIncomplete
        {
            No,
            RedirectToHTML
        }
        enum IPProtocolVersion
        {
            IPv4,
            IPv6
        }
        enum DataType
        {
            String,
            Integer,
            Decimal,
            Date,
            Datetime,
            Boolean,
            CardNumber,
            ExpiryDate,
            CardStartDate
        }
        enum MutationType
        {
            NotSet,
            Collecting,
            Processing,
            Informational,
        }
}
