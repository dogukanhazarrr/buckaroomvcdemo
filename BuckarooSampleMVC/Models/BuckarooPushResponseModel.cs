using System;
using System.Collections.Generic;

namespace BuckarooSampleMVC.Models
{
    public class BuckarooPushResponseModel
    {
        public PushResponseTransaction Transaction { get; set; }
    }
    public class PushResponseTransaction
    {
        public string Key { get; set; }
        public string Invoice { get; set; }
        public string ServiceCode { get; set; }
        public PushResponseStatus Status { get; set; }
        public bool IsTest { get; set; }
        public string Order { get; set; }
        public string Currency { get; set; }
        public decimal AmountDebit { get; set; }
        public string TransactionType { get; set; }
        public List<PushResponseService> Services { get; set; }
        public List<PushResponseServiceParameter> CustomParameters { get; set; }
        public List<PushResponseAdditionalParameter> AdditionalParameters { get; set; }
        public int MutationType { get; set; }
        public List<PushResponseRelatedTransaction> RelatedTransactions { get; set; }
        public bool IsCancelable { get; set; }
        public string IssuingCountry { get; set; }
        public bool StartRecurrent { get; set; }
        public bool Recurring { get; set; }
        public string CustomerName { get; set; }
        public string PayerHash { get; set; }
        public string PaymentKey { get; set; }
        public string Description { get; set; }
    }
    public class PushResponseStatus
    {
        public PushResponseStatusCode Code { get; set; }
        public PushResponseStatusCode SubCode { get; set; }
        public DateTime DateTime { get; set; }

    }
    public class PushResponseStatusCode
    {
        public string Code { get; set; }
        public string Description { get; set; }

    }
    public class PushResponseService
    {
        public string Name { get; set; }
        public string Action { get; set; }
        public List<PushResponseServiceParameter> Parameters { get; set; }
        public uint VersionAsProperty { get; set; }
    }
    public class PushResponseServiceParameter
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
    public class PushResponseAdditionalParameter
    {
        public List<PushResponseServiceParameter> List { get; set; }
    }
    public class PushResponseRelatedTransaction
    {
        public string RelationType { get; set; }
        public string RelatedTransactionKey { get; set; }
    }
}
