﻿using BuckarooTest.Model;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BuckarooTest
{
    class Program
    {
        static async Task Main(string[] args)
        {
            //var response = await AuthResponse();

            string apiBaseAddress = "https://testcheckout.buckaroo.nl/";

            CustomDelegatingHandler customDelegatingHandler = new();
            HttpClient client = HttpClientFactory.Create(customDelegatingHandler);


            double amount = 10;

            PaymentRequest paymentRequest = new PaymentRequest();
            //paymentRequest.Currency = "EUR";
            paymentRequest.Currency = "DKK";
            paymentRequest.AmountDebit = amount.ToString("0.00").Replace(',', '.');
            paymentRequest.Invoice = Guid.NewGuid().ToString();
            paymentRequest.ClientIP = new ClientInfo { Address = "192.168.1.1" };
            paymentRequest.ReturnURL = string.Empty;
            paymentRequest.ReturnURLCancel = string.Empty;
            paymentRequest.ReturnURLError = string.Empty;
            paymentRequest.ReturnURLReject = string.Empty;
            paymentRequest.AdditionalParameters = "";
            paymentRequest.PushURL = string.Empty;
            paymentRequest.PushURLFailure = string.Empty;
            paymentRequest.RedirectFailURL = string.Empty;

           
            paymentRequest.Services = new Service { ServiceList = new List<ServiceList>() { new ServiceList { Name = "Dankort", Action = "Pay" } } };
            
            HttpResponseMessage response = await client.PostAsJsonAsync(apiBaseAddress + "json/transaction", paymentRequest);
            var result = response.Content.ReadAsStringAsync().Result;

            PayResponse payResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<PayResponse>(result);
            var qwert = payResponse;
        }

        public class CustomDelegatingHandler : DelegatingHandler
        {
            //Obtained from the server earlier
            private string WebsiteKey = "";
            private string SecretKey = "";

            protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {
                HttpResponseMessage response = null;
                string requestContentBase64String = string.Empty;

                string requestUri = System.Web.HttpUtility.UrlEncode(request.RequestUri.Authority + request.RequestUri.PathAndQuery).ToLower();

                string requestHttpMethod = request.Method.Method;

                // calculate UNIX time
                DateTime epochStart = new DateTime(1970, 01, 01, 0, 0, 0, 0, DateTimeKind.Utc);
                TimeSpan timeSpan = DateTime.UtcNow - epochStart;
                string requestTimeStamp = Convert.ToUInt64(timeSpan.TotalSeconds).ToString();

                // create random nonce for each request
                string nonce = Guid.NewGuid().ToString("N");

                // checking if the request contains body, usually will be null with HTTP GET and DELETE
                if (request.Content != null)
                {
                    byte[] content = await request.Content.ReadAsByteArrayAsync();
                    MD5 md5 = MD5.Create();

                    // hashing the request body, any change in request body will result in different hash, we'll incure message integrity
                    byte[] requestContentHash = md5.ComputeHash(content);
                    requestContentBase64String = Convert.ToBase64String(requestContentHash);
                }

                // creating the raw signature string
                string signatureRawData = String.Format("{0}{1}{2}{3}{4}{5}", WebsiteKey, requestHttpMethod, requestUri, requestTimeStamp, nonce, requestContentBase64String);

                var secretKeyByteArray = Encoding.UTF8.GetBytes(SecretKey);

                byte[] signature = Encoding.UTF8.GetBytes(signatureRawData);

                using (HMACSHA256 hmac = new HMACSHA256(secretKeyByteArray))
                {
                    byte[] signatureBytes = hmac.ComputeHash(signature);
                    string requestSignatureBase64String = Convert.ToBase64String(signatureBytes);

                    // setting the values in the Authorization header using custom scheme (hmac)
                    request.Headers.Authorization = new AuthenticationHeaderValue("hmac", string.Format("{0}:{1}:{2}:{3}", WebsiteKey, requestSignatureBase64String, nonce, requestTimeStamp));
                }

                // set other headers
                request.Headers.Add("culture", "nl-NL");
                request.Headers.Add("channel", "Web");

                response = await base.SendAsync(request, cancellationToken);

                return response;
            }
        }

        public class PaymentRequest
        {
            public string Currency { get; set; }
            public string AmountDebit { get; set; }
            public string Invoice { get; set; }
            public ClientInfo ClientIP { get; set; }
            public Service Services { get; set; }
            public string ReturnURL { get; set; }
            public string ReturnURLCancel { get; set; }
            public string ReturnURLError { get; set; }
            public string ReturnURLReject { get; set; }
            public string PushURL { get; set; }
            public string PushURLFailure { get; set; }
            public string RedirectFailURL { get; set; }
            public string AdditionalParameters { get; set; }
        }

        public class ClientInfo
        {
            public byte Type { get; set; }
            public string Address { get; set; }
        }

        public class Service
        {
            public List<ServiceList> ServiceList { get; set; }
        }

        public class ServiceList
        {
            public string Name { get; set; }
            public string Action { get; set; }
            public List<ServiceParameters> Parameters { get; set; }
        }

        public class ServiceParameters
        {
            public string Name { get; set; }
            public string Value { get; set; }
        }
    }
}
