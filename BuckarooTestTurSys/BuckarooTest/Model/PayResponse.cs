﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuckarooTest.Model
{
    public class PayResponse
    {
        public string Key { get; set; }
        public Status Status { get; set; }
        public Requiredaction RequiredAction { get; set; }
        public object Services { get; set; }
        public object CustomParameters { get; set; }
        public object AdditionalParameters { get; set; }
        public object RequestErrors { get; set; }
        public string Invoice { get; set; }
        public object ServiceCode { get; set; }
        public bool IsTest { get; set; }
        public string Currency { get; set; }
        public float AmountDebit { get; set; }
        public object TransactionType { get; set; }
        public int MutationType { get; set; }
        public object RelatedTransactions { get; set; }
        public object ConsumerMessage { get; set; }
        public object Order { get; set; }
        public object IssuingCountry { get; set; }
        public bool StartRecurrent { get; set; }
        public bool Recurring { get; set; }
        public object CustomerName { get; set; }
        public object PayerHash { get; set; }
        public object PaymentKey { get; set; }
    }

    public class Status
    {
        public StatusCode Code { get; set; }
        public object SubCode { get; set; }
        public DateTime DateTime { get; set; }
    }

    public class StatusCode
    {
        public int Code { get; set; }
        public string Description { get; set; }
    }

    public class Requiredaction
    {
        public string RedirectURL { get; set; }
        public object RequestedInformation { get; set; }
        public object PayRemainderDetails { get; set; }
        public string Name { get; set; }
        public int TypeDeprecated { get; set; }
    }

}
